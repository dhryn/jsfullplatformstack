var jsSrc = ['./src/**/*.js'];
var lessSrc = ['./src/**/*.less'];

var outputDir = './build/';

var gulp = require('gulp');
var webpackStream = require('webpack-stream');
var compileJs = function () {
    
};
gulp.task('compile:js', function () {
    return gulp.src(jsSrc)
        .pipe(webpackStream(require('./webpack.config.js')))
        .pipe(gulp.dest(outputDir));
});

var concat = require('gulp-concat');
var lesshint = require('gulp-lesshint');
var less = require('gulp-less');
var cleanCss = require('gulp-clean-css');
gulp.task('compile:less', function () {
    return gulp.src(lessSrc)
        .pipe(concat('app.less'))
        .pipe(lesshint())
        .pipe(lesshint.reporter())
        .pipe(lesshint.failOnError())
        .pipe(less())
        .pipe(cleanCss())
        .pipe(gulp.dest(outputDir));
});

var watchJs = function () {
    return gulp.watch(jsSrc, ['compile:js']);
};
gulp.task('watch:js', watchJs);

var watchLess = function () {
    return gulp.watch(lessSrc, ['compile:less']);
};
gulp.task('watch:less', watchLess);

gulp.task('watch', function () {
    watchJs();
    watchLess();
});

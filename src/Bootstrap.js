import React from 'react';
import ReactDOM from 'react-dom';

import { TopBar } from './Component/TopBar/TopBar';

export class Bootstrap
{
    static init()
    {
        ReactDOM.render(<TopBar/>, document.getElementById('root'));
    }
}

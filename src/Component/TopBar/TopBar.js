import React from 'react';

export class TopBar extends React.Component
{
    render()
    {
        return (
            <div className="top-bar">
                <a className="btn btn-lg btn-default" href="#"><span className="glyphicon glyphicon-play"></span></a>
            </div>
        );
    }
}
